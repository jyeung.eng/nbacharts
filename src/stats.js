import { data } from 'nba.js';
import NBA from 'nba';

const teamRoster = async (year, team) => {
    try {
        let res = await data.teamRoster({ year: year, teamName: team });
        let players = res.league.standard.players;
        console.log(players);
        return res;
    } catch (err) {
        console.log(err);
    }
};

const playerStats = async (personId, year) => {
    try {
        let res = await data.playerProfile({ year: year, personId: personId });
        console.log(res);
        return res;
    } catch (err) {
        console.log(err);
    }
};

const currentStats = async (personId, year) => {
    try {
        let res = await data.playerProfile({ year: year, personId: personId });
        return res.league.standard.stats.regularSeason.season[0];
    } catch (err) {
        console.log(err);
    }
}

const findName = (name) => NBA.findPlayer(name);

export { teamRoster, playerStats, findName, currentStats };
