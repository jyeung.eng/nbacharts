import React from 'react';
import Plot from './components/Plot';
import styled from 'styled-components';
import dimensions from './styles/dimensions';
import SearchBar from './components/SearchBar';


const LayoutContainer = styled.div`
    max-width: ${dimensions.maxwidthDesktop}px;
    padding-left: ${dimensions.paddingHorizontalDesktop}em;
    padding-right: ${dimensions.paddingHorizontalDesktop}em;
    margin: 0 auto;

    @media(max-width: ${dimensions.maxwidthTablet}px) {
        padding-left: ${dimensions.paddingHorizontalTablet}em;
        padding-right: ${dimensions.paddingHorizontalTablet}em;
    }

    @media(max-width: ${dimensions.maxwidthMobile}px) {
        padding-left: ${dimensions.paddingHorizontalMobile}em;
        padding-right: ${dimensions.paddingHorizontalMobile}em;
    }

    .Layout__content {
        padding-bottom: 5em;
    }
`;

function App() {

  return (
    <LayoutContainer className="App">
      <h1>Basketball Stats</h1>
      <SearchBar />
      <div>
        <Plot />
      </div>
    </LayoutContainer>
  );
}

export default App;